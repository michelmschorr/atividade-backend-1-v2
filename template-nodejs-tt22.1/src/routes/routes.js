const { Router } = require('express');
const UserController = require('../controllers/UserController');
const ProductController = require('../controllers/ProductController');

const router = Router();


router.get('/users', UserController.index);
router.get('/users/:id', UserController.show);
router.post('/users', UserController.create);
router.put('/users/:id', UserController.update);
router.delete('/users/:id', UserController.destroy);
/*
router.put('/usersaddproduct/:id', UserController.addRelationProduct);
router.delete('/usersremoveproduct/:id', UserController.removeRelationProduct);
*/

router.get('/products', ProductController.index);
router.get('/products/:id', ProductController.show);
router.post('/products', ProductController.create);
router.put('/products/:id', ProductController.update);
router.delete('/products/:id', ProductController.destroy);
router.put('/productsadduser/:id', ProductController.addRelationUser);
router.delete('/productsremoveuser/:id', ProductController.removeRelationUser);

module.exports = router;



