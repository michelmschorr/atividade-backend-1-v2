const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    phoneNumber: {
        type: DataTypes.STRING,
        allowNull: false
    },
    
    cep: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    
    creditCard: {
        type: DataTypes.INTEGER,
        
    }
    
    
},
{
    timestamps: false
});




User.associate = function(models) {
    User.hasMany(models.Product);
}

module.exports = User;