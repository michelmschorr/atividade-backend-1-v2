const { response } = require('express');
const User = require('../models/User');
const Product = require('../models/Product');

const index = async(req,res) => {
    try {
        const users = await User.findAll();
        return res.status(200).json({users});
    } catch (error) {
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {

    const {id} = req.params;

    try {
        
        const user = await User.findByPk(id);
        return res.status(200).json({user});
    } catch (error) {
        return res.status(500).json({error});
    }
};

const create = async(req, res) => {
    try {
        const user = User.create(req.body);
        return res.status(201).json({message: "Usuario cadastrado com sucesso", user: user});
    } catch (error) {
        return res.status(500).json({error: error});
    }
};

/*
const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await User.update(req.body, {where: {id: id}});
        if(updated){
            const user = User.findByPk(id);
            return res.status(200).send(user);
        }
        throw new Error();
    } catch (error) {
        return res.status(500).json("Usuario não encontrado");
    }
}
*/
const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await User.update(req.body, {where: {id: id}});
        if(updated) {
            const user = await User.findByPk(id);
            console.log(user);
            return res.status(200).send(user);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Usuário não encontrado");
    }
};


const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await User.destroy({where: {id: id}});
        if(deleted){
            return res.status(200).json("Usuario deletado com sucesso");
        }
    
        throw new Error();
    
    } catch (error) {
        return res.status(500).json("Usuario não encontrado");
    }
}
/*
const addRelationProduct = async(req, res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        const product = await Product.findByPk(req.body.ProductId);
        await user.addProduct(product);
        return res.status(200).json(user);
    } catch (error) {
        return res.status(500).json({error});
    }
}
*/
/*eh assim msm?*/
/*
const addRelationProducts = async(req, res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        const products = await Product.findAll({where: {id: req.body.ProductIds}});
        await user.addProducts(products);
        return res.status(200).json(user);
    } catch (error) {
        return res.status(500).json({err});
    }
}
*/
/*
const removeRelationProduct = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        const product = await Product.findByPk(req.body.ProductId);
        await user.removeProduct(product);
        return res.status(200).json(user);
    } catch (error) {
        return res.status(500).json({error});
    }
}
*/

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    /*
    addRelationProduct,
    removeRelationProduct
    */
};
