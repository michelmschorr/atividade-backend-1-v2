const { response } = require('express');
const User = require('../models/User');
const Product = require('../models/Product');

const index = async(req,res) => {
    try {
        const products = await Product.findAll();
        return res.status(200).json({products});
    } catch (error) {
        return res.status(500).json({error});
    }
};

const show = async(req,res) => {

    const {id} = req.params;

    try {
        
        const product = await Product.findByPk(id);
        return res.status(200).json({product});
    } catch (error) {
        return res.status(500).json({error});
    }
};

const create = async(req, res) => {
    try {
        const product = Product.create(req.body);
        return res.status(201).json({message: "Usuario cadastrado com sucesso", product: product});
    } catch (error) {
        return res.status(500).json({error: error});
    }
};


const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Product.update(req.body, {where: {id: id}});
        if(updated){
            const product = Product.findByPk(id);
            return res.status(200).send(product);
        }
        throw new Error();
    } catch (error) {
        return res.status(500).json("Usuario não encontrado");
    }
}

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Product.destroy({where: {id: id}});
        if(deleted){
            return res.status(200).json("Usuario deletado com sucesso");
        }
    
        throw new Error();
    
    } catch (error) {
        return res.status(500).json("Usuario não encontrado");
    }
}

const addRelationUser = async(req, res) => {
    const {id} = req.params;
    try {
        const product = await Product.findByPk(id);
        const user = await User.findByPk(req.body.UserId);
        
        await product.setUser(user);
        return res.status(200).json(product);
    } catch (error) {
        return res.status(500).json({error});
    }
}

/*eh assim msm?*/
/*
const addRelationProducts = async(req, res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        const products = await Product.findAll({where: {id: req.body.ProductIds}});
        await user.addProducts(products);
        return res.status(200).json(user);
    } catch (error) {
        return res.status(500).json({err});
    }
}




*/

const removeRelationUser = async(req,res) => {
    const {id} = req.params;
    try {
        const product = await Product.findByPk(id);
        const user = await User.findByPk(req.body.UserId);
        
        await product.removeUser(user);
        return res.status(200).json(product);
    } catch (error) {
        return res.status(500).json({error});
    }
}

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addRelationUser,
    removeRelationUser
};