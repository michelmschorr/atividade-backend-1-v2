# Atividade Backend 1

Repositorio para a atividade de Backend 1, versao funcionando

PS: o repositorio https://gitlab.com/michelmschorr/atividade-backend-1 tem as branches todas completas do devenvolvimento, porem o projeto teve varios erros na sua montagem inicial. Criei um novo projeto, que esta funcionando, nesse repositorio.

## Instalacao

Clonar o repositorio com o seguinte comando

```bash
git clone https://gitlab.com/michelmschorr/atividade-backend-1-v2.git main
```


## Uso

```bash
npm run migrate
npm run dev
```